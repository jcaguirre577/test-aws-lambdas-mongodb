# test-aws-lambda-mongodb
Este ejemplo demuestra cómo implementar una función de NodeJS que se ejecuta en AWS Lambda utilizando el marco sin servidor tradicional . Las funciones desplegadas funcionan con MongoDB Atlas .

Este ejemplo funciona con eventos de la API HTTP de AWS . Para una respuesta más rápida con las API , se utiliza el almacenamiento en caché de Redis . Para el seguimiento de sesiones se utiliza JSON Web Token (JWT) .

Todos los registros de la función se mantienen en AWS Cloudwatch, es decir , persistentes .

Para usar el código en este ejemplo, debe tener una cuenta de AWS válida y los roles de IAM de AWS necesarios y acceso programático a un usuario. Debe tener una cuenta de MongoDB Atlas y una instancia de AWS EC2 debe estar instalada con Redis .

Características
Función AWS Lambda usando NodeJS
La función utiliza la última versión de AWS SDK JavaScript v3 con todas las sintaxis de ES6+ como Promises,async/await
La función se implementa utilizando Serverless Framework.
serverless.jsonse utiliza para la configuración de implementación en lugar de serverless.yml.
Toda la implementación se crea en AWS S3 para almacenar el .zipcódigo de la función y la pila de AWS CloudFormation .
Para el seguimiento de sesiones se utiliza JWT .
La API HTTP de AWS utiliza AWS API GateWay
Todos los datos se guardan en MongoDB Atlas , es decir , persistente
El almacenamiento en caché se utiliza para una respuesta más rápida en las API. Redis se utiliza para ese propósito
Estas API también pueden ser consumidas por cualquier aplicación frontend .
Las dependencias de NPM se utilizan para diversos fines.
Los encabezados personalizados se agregan con la respuesta por razones obvias de seguridad .
Uso
---------------------------------

$ cd test-aws-lambda-mongodb
$ npm install
Despliegue
Para implementar el ejemplo, debe ejecutar el siguiente comando:

$ serverless deploy
Invocación
Después de una implementación exitosa, puede invocar la función implementada. Todos los croneventos invocarán las funciones desplegadas en el intervalo de tiempo estipulado.

Sin embargo, para llamar a using httpApi, puede usar cualquier cliente REST como Talend API Tester con los verbos HTTPurl como se muestra en la Terminal después de usar .serverless deploy

Listado de API
POST /login tiene la siguiente entrada json

{
  "token": "xxxxxxxxxxxxxxxxxxx",
  
} 
Las siguientes API deben contener Authorization: Bearer <token>en los encabezados

POST /adduser tiene la siguiente entrada json

{
  "email": "123456@gmail.com",
  "card_number": "12345678978945",
  "cvc": "123",
  "expiration_year": "2025",
  "expiration_month": "05",

} 


  